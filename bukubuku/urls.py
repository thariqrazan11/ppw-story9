from django.contrib import admin
from django.urls import path

from bukubuku import views

urlpatterns = [
    path('', views.index, name="landing_page"),
    path('api/v1/likebook/', views.api_like_book, name="api_like_book"),
    path('api/v1/unlikebook/', views.api_unlike_book, name="api_unlike_book"),
    path('api/v1/topbook/', views.top_book, name="top_books"),
]
