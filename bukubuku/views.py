from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse, HttpResponseBadRequest
import json

from .models import ModelBuku

# Create your views here.

def index(request):
    return render(request, 'indexBukubuku.html')

@csrf_exempt
def api_like_book(request):
    data = json.loads(request.body)
    if 'etag' in data: # 'etag' adanya di hasil query, sehingga data buku diambil dari hasil query tersebut
        if 'authors' not in data['volumeInfo']:
            data['volumeInfo']['authors'] = [None]

        if 'publisher' not in data['volumeInfo']:
            data['volumeInfo']['publisher'] = None
        
        if 'averageRating' not in data['volumeInfo']:
            data['volumeInfo']['averageRating'] = None

        if 'imageLinks' not in data['volumeInfo']:
            data['volumeInfo']['imageLinks'] = {
                'thumbnail' : ''
            }

        try:
            book = ModelBuku.objects.get(book_id=data['id'])
            book.like_count += 1
            book.save()
        except ModelBuku.DoesNotExist:
            ModelBuku.objects.create(
                book_id = data['id'],
                title = data['volumeInfo']['title'],
                authors = data['volumeInfo']['authors'][0],
                publisher = data['volumeInfo']['publisher'],
                average_rating = data['volumeInfo']['averageRating'],
                image_link = data['volumeInfo']['imageLinks']['thumbnail'],
                like_count = 1,
            )
        return HttpResponse()
    else: # data diambil dari model, ini untuk 'Top Buku (favorit)'
        book = ModelBuku.objects.get(book_id=data['id'])
        book.like_count += 1
        book.save()
        json_data = {
            'likeCount' : book.like_count
        }
        return JsonResponse(json_data)

def top_book(request):
    top_book = ModelBuku.objects.order_by('-like_count')[:5]
    data = []
    for book in top_book:
        data.append({
            'bookId' : book.book_id,
            'title' : book.title,
            'publisher' : book.publisher,
            'imageLinks' : book.image_link,
            'likeCount' : book.like_count,
        })
    json_data = {
        'topBook' : data
    }
    return JsonResponse(json_data, safe=False)

@csrf_exempt
def api_unlike_book(request):
    data = json.loads(request.body)
    try:
        book = ModelBuku.objects.get(book_id=data['id'])
        book.like_count -= 1
        book.save()
        json_data = {
            'likeCount' : book.like_count
        }
        return JsonResponse(json_data)
    except:
        return HttpResponseBadRequest()
