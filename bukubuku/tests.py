from django.test import TestCase, Client, LiveServerTestCase
from django.http import HttpRequest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from faker import Faker

import os
import time
import random
import string

from .models import ModelBuku
# from .views import namaFungsinya

# Create your tests here.

class UnitTest(TestCase):
    def test_landing_page_is_found(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_ModelBuku(self):
        letters = string.ascii_letters
        random_book_id = ''.join(random.choice(letters) for i in range(10))
        random_like_count = random.randint(1,10)
        faker = Faker()
        title = faker.text()
        publisher = faker.name()
        ModelBuku.objects.create(
            book_id = random_book_id,
            title = title,
            authors = None,
            publisher = publisher,
            average_rating = None,
            image_link = '',
            like_count = random_like_count,
        )

        counter = ModelBuku.objects.all().count()
        self.assertEqual(counter, 1)

        data = ModelBuku.objects.get(title=title)
        self.assertEqual(str(data), title + ' - ' + random_book_id)


class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super(FunctionalTest, self).setUp()

        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome(chrome_options=chrome_options)        

    def tearDown(self):
        self.browser.quit()

        super(FunctionalTest, self).tearDown()

    def test_search_result_shown(self):
        self.browser.get(self.live_server_url + '/')
        time.sleep(5)
        
        query_input = self.browser.find_element_by_xpath("//input[@id='searchBox']")
        query_input.send_keys("The Subtle Art of Not Giving a F*ck")
        time.sleep(15) # Wait web page to process and showing result

        book_item = self.browser.find_element_by_class_name('book-item')
        book_data = book_item.get_attribute('innerHTML')

        self.assertIn('F*ck', book_data) # Check wether book data shown in the web page
    
    def test_search_result_shown_2(self):
        # Data from the source is incomplete
        self.browser.get(self.live_server_url + '/')
        time.sleep(5)

        query_input = self.browser.find_element_by_xpath("//input[@id='searchBox']")
        query_input.send_keys("Bluetongue - AGDPT, CFT, Microtitre Neutralisation Test") # NO PUBLISHER DATA
        query_input.send_keys(" ")
        time.sleep(15)

        book_item = self.browser.find_element_by_class_name('book-item')
        book_data = book_item.get_attribute('innerHTML')
        self.assertIn('Bluetongue', book_data) # Check wether book data shown in the web page
  
    def test_most_liked_book_shown_in_modal(self):
        books_added = []
        # Create 10 book instance
        for i in range(10):
            letters = string.ascii_letters
            random_book_id = ''.join(random.choice(letters) for i in range(10))
            random_like_count = random.randint(1,10)
            faker = Faker()
            title = faker.text()
            publisher = faker.name()
            ModelBuku.objects.create(
                book_id = random_book_id,
                title = title,
                authors = None,
                publisher = publisher,
                average_rating = None,
                image_link = '',
                like_count = random_like_count,
            )

            books_added.append((title, random_like_count))

        books_added = sorted(books_added, key = lambda books : books[1], reverse = True)
        top_5_book = books_added[0:5]
        self.browser.get(self.live_server_url + '/')
        time.sleep(20)
        
        modal_button = self.browser.find_element_by_id('modalBtn')
        modal_button.click()
        time.sleep(15)

        modal_data = self.browser.find_element_by_id('topBookModal').get_attribute('innerHTML')
        for x in top_5_book:
            self.assertTrue(x[0] in modal_data)
        
        for i in range(len(top_5_book) -1 ):
            self.assertTrue(modal_data.index(top_5_book[i][0]) < modal_data.index(top_5_book[i+1][0]))



    # def test_unlike_book(self):
    #     driver = webdriver.Chrome()
    #     self.browser.get(self.live_server_url + '/')
    #     time.sleep(5)

    #     query_input = self.browser.find_element_by_xpath("//input[@id='searchBox'][1]")
    #     query_input.send_keys("Harry Potter dan tawanan Azkaban gramedia pustaka utama")
    #     time.sleep(10)

    #     # book_title = self.browser.find_element_by_class_name('book-title').get_attribute('innerHTML')
    #     # wait = WebDriverWait(driver, 10)
    #     # wait.until(EC.visibility_of_element_located((book_title)))
        

    #     book_like_button = self.browser.find_element_by_class_name('book-like-btn')
    #     book_like_button.click()
    #     time.sleep(60)
        
    #     books = ModelBuku.objects.get(title='Harry Potter dan tawanan Azkaban')
    #     self.assertEqual(books.like_count , 1)

    #     book_like_button.click()
    #     time.sleep(60)
        
    #     books = ModelBuku.objects.get(title='Harry Potter dan tawanan Azkaban')
    #     self.assertEqual(books.like_count , 0)



    # def test_unlike_book_not_exist(self):
    #     self.browser.get(self.live_server_url + '/')
    #     time.sleep(5)

    #     query_input = self.browser.find_element_by_xpath("//input[@id='searchBox'][1]")
    #     query_input.send_keys("Harry Potter dan tawanan Azkaban gramedia")
    #     time.sleep(15)
    #     query_input.send_keys(" ")
    #     time.sleep(15)

    #     book_title = self.browser.find_element_by_class_name('book-title').get_attribute('innerHTML')
    #     book_like_button = self.browser.find_element_by_class_name('book-like-btn')

    #     book_like_button.click()
    #     time.sleep(15)
        
    #     # Deleted in case of failed db migrations or db change
    #     books = ModelBuku.objects.get(title='Harry Potter dan tawanan Azkaban').delete()

    #     book_like_button.click()
    #     time.sleep(15)
        
    #     book_like_button = self.browser.find_element_by_class_name('book-like-btn').get_attribute('innerHTML')
    #     self.assertEqual(book_like_button , 'ERROR')

    # def test_like_from_top_books(self):
    #     books_added = []
    #     # Create 10 book instance
    #     for _ in range(10):
    #         letters = string.ascii_letters
    #         random_book_id = ''.join(random.choice(letters) for i in range(10))
    #         random_like_count = random.randint(1,10)
    #         faker = Faker()
    #         title = faker.text()
    #         publisher = faker.name()
    #         ModelBuku.objects.create(
    #             book_id = random_book_id,
    #             title = title,
    #             publisher = publisher,
    #             like_count = random_like_count,
    #             image_link = '',
    #             authors = None,
    #             average_rating = None
    #         )

    #         books_added.append((title, random_like_count))

    #     books_added = sorted(books_added, key = lambda books : books[1], reverse = True)
    #     top_5_book = books_added[0:5]

    #     before_like_count = ModelBuku.objects.get(title=top_5_book[0][0]).like_count
        
    #     self.browser.get(self.live_server_url + '/')
    #     time.sleep(5)
    #     top_book_button = self.browser.find_element_by_id('modalBtn')
    #     top_book_button.click()
    #     time.sleep(5)

    #     like_button_top_book = self.browser.find_element_by_xpath("//div[@id='topBookContainer']/div[1]/div/button")
    #     like_button_top_book.click()
    #     time.sleep(5)

    #     after_like_count = ModelBuku.objects.get(title=top_5_book[0][0]).like_count

    #     self.assertTrue(after_like_count == before_like_count+1)



    # def test_like_unlike_button(self):
    #     self.browser.get(self.live_server_url + '/')
    #     time.sleep(5)

    #     query_input = self.browser.find_element_by_xpath("//input[@id='searchBox']")
    #     query_input.send_keys("Harry Potter dan tawanan Azkaban gramedia pustaka utama")
    #     # time.sleep(2)
    #     # query_input.send_keys(" ")
    #     time.sleep(15)

    #     # book_title = self.browser.find_element_by_class_name('book-title').get_attribute('innerHTML')
    #     # book_like_button = self.browser.find_element_by_class_name('book-like-btn')
    #     like_button = self.browser.find_element_by_class_name(('book-like-btn')[1])
    #     like_button.click()


    #     # book_like_button.click()
    #     time.sleep(5)
        
    #     books = ModelBuku.objects.get(title='Harry Potter dan tawanan Azkaban')
    #     self.assertEqual(books.like_count , 1)

    #     # book_like_button.click()
    #     like_button.click()
    #     time.sleep(5)
        
    #     books = ModelBuku.objects.get(title='Harry Potter dan tawanan Azkaban')
    #     self.assertEqual(books.like_count , 0)

    
    
    # def test_like_unlike(self):
    #     self.browser.get(self.live_server_url + '/')
    #     time.sleep(5)

    #     query_input = self.browser.find_element_by_xpath("//input[@id='searchBox']")
    #     query_input.send_keys("Harry Potter dan tawanan Azkaban gramedia pustaka utama")
    #     time.sleep(2)
    #     query_input.send_keys(" ")
    #     time.sleep(15)

    #     book_like_button = self.browser.find_element_by_class_name('book-like-btn')

    #     book_like_button.click() # First click for 'like'
    #     time.sleep(5)
        
    #     books = ModelBuku.objects.get(title='Harry Potter dan tawanan Azkaban')
    #     self.assertEqual(books.like_count , 1)

    #     book_like_button.click() # Second click for 'unlike'
    #     time.sleep(5)
        
    #     books = ModelBuku.objects.get(title='Harry Potter dan tawanan Azkaban')
    #     self.assertEqual(books.like_count , 0)




    # def test_like_unlike(self):
    #     self.browser.get(self.live_server_url + '/')
    #     time.sleep(5)

    #     query_input = self.browser.find_element_by_xpath("//input[@id='searchBox']")
    #     query_input.send_keys("Lima sekawan di Pulau Seram")
    #     time.sleep(5)
    #     query_input.send_keys(" ")
    #     time.sleep(15)

    #     book_title = self.browser.find_element_by_class_name('book-title').get_attribute('innerHTML')
    #     book_like_button = self.browser.find_element_by_class_name('book-like-btn')

    #     book_like_button.click() # First click for 'like'
    #     time.sleep(5)
        
    #     books = ModelBuku.objects.get(title="Lima sekawan di Pulau Seram")
    #     self.assertEqual(books.like_count , 1) # Check wether 'like button' works

    #     book_like_button.click() # Second click for 'unlike'
    #     time.sleep(5)
        
    #     books = ModelBuku.objects.get(title="Lima sekawan di Pulau Seram")
    #     self.assertEqual(books.like_count , 0) # Check wether 'like button' works

    # def test_unlike_book_not_exist(self):
    #     self.browser.get(self.live_server_url + '/')
    #     time.sleep(5)

    #     query_input = self.browser.find_element_by_xpath("//input[@id='searchBox'][1]")
    #     query_input.send_keys("Lima sekawan di Pulau Seram")
    #     time.sleep(5)
    #     query_input.send_keys(" ")
    #     time.sleep(15)

    #     book_like_button = self.browser.find_element_by_class_name('book-like-btn')

    #     book_like_button.click()
    #     time.sleep(5)
        
    #     # Deleted in case of failed db migrations or db change
    #     books = ModelBuku.objects.get(title="Lima sekawan di Pulau Seram").delete()

    #     book_like_button.click()
    #     time.sleep(15)
        
    #     book_like_button = self.browser.find_element_by_class_name('book-like-btn').get_attribute('innerHTML')
    #     self.assertEqual(book_like_button , 'ERROR')


