from django.db import models

# Create your models here.

class ModelBuku(models.Model):
    max_length = 64
    book_id = models.CharField(max_length=max_length, unique=True)
    title = models.CharField(max_length=max_length)
    authors = models.CharField(max_length=max_length, null=True)
    publisher = models.CharField(max_length=max_length, null=True)
    average_rating = models.IntegerField(null=True)
    image_link = models.TextField()
    like_count = models.IntegerField(default=0)

    def __str__(self):
        return self.title + " - " + self.book_id
