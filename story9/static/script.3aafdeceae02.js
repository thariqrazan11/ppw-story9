var bookShowed;
var likedBook;
var queryCheckSum;
var topBookShowed = [];
var likedBookShowed = [];

// Get likedBook data from localstorage to variable
if (localStorage.getItem('likedBook') != null) {
  var jsonLikedBook = JSON.parse(localStorage.getItem('likedBook'));
  likedBook = jsonLikedBook['likedBook'];
} else {
  likedBook = [];
  var jsonLikedBook = { 'likedBook': likedBook };
  localStorage.setItem('likedBook', JSON.stringify(jsonLikedBook));
}

// Query find book
$(document).ready(function () {
  $('#searchBox').on('input', function () {
    var query = $(this).val();
    if (query.length > 3) {
      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            // Set query check sum to result query
            queryCheckSum = query;
            $('.book-result-container').html('');
            var data = JSON.parse(this.responseText);
            if ('items' in data) {
              bookShowed = data['items']
              var bookHTML;
              for (var i = 0; i < data['items'].length; i++) {
                var book = data['items'][i];
                bookHTML = '';
                bookHTML += '<div class="book-item">';
                if ('imageLinks' in book['volumeInfo']) {
                  bookHTML += '<img class="book-thumbnail" src="' + book['volumeInfo']['imageLinks']['thumbnail'] + '">';
                }
                bookHTML += '<div class="book-detail">';
                bookHTML += '<div class="book-title">' + book['volumeInfo']['title'] + '</div>';
                bookHTML += '<div class="book-publisher">' + book['volumeInfo']['publisher'] + '</div>';
                bookHTML += '<button type="button" class="book-like-button" onclick="saveBook(this, ' + i + ')">';
                if (likedBook.includes(book['id'])) {
                  bookHTML += '<i class="fas fa-heart"></i> <span class="like-text">LIKED</span>';
                } else {
                  bookHTML += '<i class="far fa-heart"></i> <span class="like-text">LIKE</span>';
                }
                bookHTML += '<div class="lds-ring"><div></div><div></div><div></div>';
                bookHTML += '</button></div></div>';
                $('#bookResult').append(bookHTML);
              }
            } else {
              $('.book-result-container').html('Tidak menemukan <b>"'+query+'"</b>');
            }
        }
      };
      xhttp.open("GET", "https://www.googleapis.com/books/v1/volumes?q=" + query, true);
      xhttp.send();
    }
  });

  setInterval(queryCheck(), 500);

  loadBooksLoved()
});

// Polling interval 500ms to check query result equals to search input
function queryCheck() {
  var query = $('#searchBox').val();
  if (query != '' && query.length > 3 && query != queryCheckSum) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState == 4) {
        if (this.status == 200) {
          queryCheckSum = query;
          $('.book-result-container').html('');
          var data = JSON.parse(this.responseText);
          if ('items' in data){
            bookShowed = data['items']
            var bookHTML;
            for (var i = 0; i < data['items'].length; i++) {
              var book = data['items'][i];
              bookHTML = '';
              bookHTML += '<div class="book-item">';
              if ('imageLinks' in book['volumeInfo']) {
                bookHTML += '<img class="book-thumbnail" src="' + book['volumeInfo']['imageLinks']['thumbnail'] + '">';
              }
              bookHTML += '<div class="book-detail">';
              bookHTML += '<div class="book-title">' + book['volumeInfo']['title'] + '</div>';
              bookHTML += '<div class="book-publisher">' + book['volumeInfo']['publisher'] + '</div>';
              bookHTML += '<button type="button" class="book-like-button" onclick="saveBook(this, ' + i + ')">';
              if (likedBook.includes(book['id'])) {
                bookHTML += '<i class="fas fa-heart"></i> <span class="like-text">LIKED</span>';
              } else {
                bookHTML += '<i class="far fa-heart"></i> <span class="like-text">LIKE</span>';
              }
              bookHTML += '<div class="lds-ring"><div></div><div></div><div></div>';
              bookHTML += '</button></div></div>';
              $('#bookResult').append(bookHTML);
            }
          }else{
            $('.book-result-container').html('Tidak menemukan <b>"'+query+'"</b>');
          }
        }
      }
    };
    xhttp.open("GET", "https://www.googleapis.com/books/v1/volumes?q=" + query, true);
    xhttp.send();
  }
}

// Like book from showed book and ajax request to django
function saveBook(e, bookIndex) {
  var bookData = bookShowed[bookIndex];
  // Prevent multiple like for each user (browser)
  if (likedBook.includes(bookData['id'])) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      $(e).children('.lds-ring').css('display', 'inline-block');
      $(e).children('.like-text').css('display', 'none');
      if (this.readyState == 4) {
        $(e).children('.lds-ring').css('display', 'none');
        $(e).children('.like-text').css('display', 'inline-block');
        if (this.status == 200) {
          e.innerHTML = '<i class="far fa-heart"></i> <span class="like-text">LIKE</span>';
          e.innerHTML += '<div class="lds-ring"><div></div><div></div><div></div>';
          var savedBookIndex = likedBook.indexOf(bookData['id'])
          likedBook.splice(savedBookIndex, 1);
          saveLocalData();
          loadBooksLoved();
        } else {
          e.innerHTML = 'ERROR';
        }
      }
    };
    xhttp.open("POST", "/api/v1/unlikebook/", true);

    var bookDataJSON = JSON.stringify(bookData);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send(bookDataJSON);
  } else {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      $(e).children('.lds-ring').css('display', 'inline-block');
      $(e).children('.like-text').css('display', 'none');
      if (this.readyState == 4) {
        $(e).children('.lds-ring').css('display', 'none');
        $(e).children('.like-text').css('display', 'inline-block');
        if (this.status == 200) {
          e.innerHTML = '<i class="fas fa-heart"></i> <span class="like-text">LIKED</span>';
          e.innerHTML += '<div class="lds-ring"><div></div><div></div><div></div>';
          likedBook.push(bookData['id']);
          saveLocalData();
          loadBooksLoved();
        } else {
          e.innerHTML = 'ERROR';
        }
      }
    };
    xhttp.open("POST", "/api/v1/likebook/", true);

    var bookDataJSON = JSON.stringify(bookData);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send(bookDataJSON);
  }
}

function loadTopBook() {
  $('#topBookContainer').html('');
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4) {
      if (this.status == 200) {
        topBook = []
        var data = JSON.parse(this.responseText);
        var topBook = data['topBook'];
        var bookHTML;
        for (var i = 0; i < topBook.length; i++) {
          var book = topBook[i];
          topBookShowed.push(book['bookId']);
          bookHTML = '';
          bookHTML += '<div class="book-item">';
          bookHTML += '<img class="book-thumbnail" src="' + book['imageLinks'] + '">';
          bookHTML += '<div class="book-detail">';
          bookHTML += '<div class="book-title">' + book['title'] + '</div>';
          bookHTML += '<div class="book-publisher">' + book['publisher'] + '</div>';
          bookHTML += '<button type="button" class="book-like-button" onclick="likeTopBook(this, ' + i + ')">';
          if (likedBook.includes(book['bookId'])) {
            bookHTML += book['likeCount'] + '  <i class="fas fa-heart"></i> <span class="like-text">LIKED</span>';
          } else {
            bookHTML += book['likeCount'] + '  <i class="far fa-heart"></i> <span class="like-text">LIKE</span>';
          }
          bookHTML += '<div class="lds-ring"><div></div><div></div><div></div>';
          bookHTML += '</button>';
          bookHTML += '</div></div>';
          $('#topBookContainer').append(bookHTML);
        }
      }
    }
  };
  xhttp.open("GET", "/api/v1/topbook/", true);
  xhttp.send();
}

function likeTopBook(e, bookIndex) {
  if (likedBook.includes(topBookShowed[bookIndex])) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      $(e).children('.lds-ring').css('display', 'inline-block');
      $(e).children('.like-text').css('display', 'none');
      if (this.readyState == 4) {
        $(e).children('.lds-ring').css('display', 'none');
        $(e).children('.like-text').css('display', 'inline-block');
        if (this.status == 200) {
          likeBookCount = JSON.parse(this.responseText)['likeCount'];
          e.innerHTML = likeBookCount + '  <i class="far fa-heart"></i> <span class="like-text">LIKE</span>';
          var savedBookIndex = likedBook.indexOf(topBookShowed[bookIndex])
          likedBook.splice(savedBookIndex, 1);
          saveLocalData();
          loadBooksLoved();
        } else {
          e.innerHTML = 'ERROR';
        }
      }
    };
    xhttp.open("POST", "/api/v1/unlikebook/", true);
    var bookData = {
      'id': topBookShowed[bookIndex]
    };
    var bookDataJSON = JSON.stringify(bookData);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send(bookDataJSON);
  } else {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      $(e).children('.lds-ring').css('display', 'inline-block');
      $(e).children('.like-text').css('display', 'none');
      if (this.readyState == 4) {
        $(e).children('.lds-ring').css('display', 'none');
        $(e).children('.like-text').css('display', 'inline-block');
        if (this.status == 200) {
          likeBookCount = JSON.parse(this.responseText)['likeCount'];
          e.innerHTML = likeBookCount + '  <i class="fas fa-heart"></i> <span class="like-text">LIKED</span>';
          likedBook.push(topBookShowed[bookIndex]);
          saveLocalData();
          loadBooksLoved();
        } else {
          e.innerHTML = 'ERROR';
        }
      }
    };
    xhttp.open("POST", "/api/v1/likebook/", true);
    var bookData = {
      'id': topBookShowed[bookIndex]
    };
    var bookDataJSON = JSON.stringify(bookData);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send(bookDataJSON);
  }
}

function loadBooksLoved() {
  $('.book-liked-container').html('');
  for (var i = 0; i < likedBook.length && i < 5; i++) {
    likedBookShowed.push(likedBook[i]);
    var xhttp = new XMLHttpRequest();
    xhttp.i = i;
    var bookHTML;
    xhttp.onreadystatechange = function () {
      if (this.readyState == 4) {
        if (this.status == 200) {
          book = JSON.parse(this.responseText);
          bookHTML = '';
          bookHTML += '<div class="book-item">';
          if ('imageLinks' in book['volumeInfo']) {
            bookHTML += '<img class="book-thumbnail" src="' + book['volumeInfo']['imageLinks']['thumbnail'] + '">';
          }
          bookHTML += '<div class="book-detail">';
          bookHTML += '<div class="book-title">' + book['volumeInfo']['title'] + '</div>';
          bookHTML += '<div class="book-publisher">' + book['volumeInfo']['publisher'] + '</div>';
          bookHTML += '<button type="button" class="book-like-button" onclick="likeLovedBook(this, ' + this.i + ')">';
          if (likedBook.includes(book['id'])) {
            bookHTML += '<i class="fas fa-heart"></i> <span class="like-text">LIKED</span>';
          } else {
            bookHTML += '<i class="far fa-heart"></i> <span class="like-text">LIKE</span>';
          }
          bookHTML += '<div class="lds-ring"><div></div><div></div><div></div>';
          bookHTML += '</button></div></div>';
          $('.book-liked-container').append(bookHTML);
        }
      }
    }
    xhttp.open("GET", "https://www.googleapis.com/books/v1/volumes/" + likedBook[i], true);
    xhttp.send();
  }
}

function likeLovedBook(e, bookIndex) {
  if (likedBook.includes(likedBookShowed[bookIndex])) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      $(e).children('.lds-ring').css('display', 'inline-block');
      $(e).children('.like-text').css('display', 'none');
      if (this.readyState == 4) {
        $(e).children('.lds-ring').css('display', 'none');
        $(e).children('.like-text').css('display', 'inline-block');
        if (this.status == 200) {
          likeBookCount = JSON.parse(this.responseText)['likeCount'];
          e.innerHTML = '<i class="far fa-heart"></i> <span class="like-text">LIKE</span>';
          e.innerHTML += '<div class="lds-ring"><div></div><div></div><div></div>';
          var savedBookIndex = likedBook.indexOf(topBookShowed[bookIndex])
          likedBook.splice(savedBookIndex, 1);
          saveLocalData();
        } else {
          e.innerHTML = 'ERROR';
        }
      }
    };
    xhttp.open("POST", "/api/v1/unlikebook/", true);
    var bookData = {
      'id': likedBookShowed[bookIndex]
    };
    var bookDataJSON = JSON.stringify(bookData);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send(bookDataJSON);
  } else {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      $(e).children('.lds-ring').css('display', 'inline-block');
      $(e).children('.like-text').css('display', 'none');
      if (this.readyState == 4) {
        $(e).children('.lds-ring').css('display', 'none');
        $(e).children('.like-text').css('display', 'inline-block');
        if (this.status == 200) {
          likeBookCount = JSON.parse(this.responseText)['likeCount'];
          e.innerHTML = '<i class="fas fa-heart"></i> <span class="like-text">LIKED</span>';
          e.innerHTML += '<div class="lds-ring"><div></div><div></div><div></div>';
          likedBook.push(likedBookShowed[bookIndex]);
          saveLocalData();
        } else {
          e.innerHTML = 'ERROR';
        }
      }
    };
    xhttp.open("POST", "/api/v1/likebook/", true);
    var bookData = {
      'id': likedBookShowed[bookIndex]
    };
    var bookDataJSON = JSON.stringify(bookData);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send(bookDataJSON);
  }
}

function saveLocalData() {
  localStorage.setItem('likedBook', JSON.stringify({ 'likedBook': likedBook }));
}
